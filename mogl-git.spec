Name: mogl-git
Version: 0.1.4.r0.g7c181fd
Release: 1
Summary: 'Modern OpenGL wrapper, thin C++14 header-only layer on top of the OpenGL 4.5+ API'
License: MIT
Provides: mogl
Conflicts: mogl
BuildRequires: git
BuildRequires: cmake
#Source0: git+https://github.com/Ryp/moGL.git
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%global _gitname moGL
%define pkgname mogl-git

%prep
 

%description
'Modern OpenGL wrapper, thin C++14 header-only layer on top of the OpenGL 4.5+ API'

%build
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd "$srcdir/%{_gitname}";
    mkdir -p build && pushd build;
    cmake -DCMAKE_INSTALL_PREFIX=/usr ..;
    make

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd "$srcdir/%{_gitname}/build";
    make install DESTDIR="$pkgdir"

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

